import React, { Component } from 'react'
import PropTypes from 'prop-types'

import defsettings from '../settings.json'

class Message extends Component {
  constructor(props) {
    super(props)

    this.state = {
      settings: { text: { en: '', ar: '' }, announcement: '' } || defsettings,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.settings !== this.state.settings && nextProps.settings !== null) {
      this.setState({ settings: nextProps.settings })
    }
  }

  render() {
    const announcement = this.state.settings.announcement ? <h3>{this.state.settings.announcement}</h3> : ''
    const ar = this.state.settings.text.ar ? <div>{this.state.settings.text.ar}</div> : null
    const en = this.state.settings.text.en ? <div>{this.state.settings.text.en}</div> : null

    return (
      <div className="Message" ref={(divElement) => (this.divElement = divElement)}>
        {announcement}
        {en}
        {ar}
      </div>
    )
  }
}

Message.propTypes = {
  settings: PropTypes.object,
}

export default Message
