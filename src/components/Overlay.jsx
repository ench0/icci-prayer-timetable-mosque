import React, { Component } from 'react'
import PropTypes from 'prop-types'

import moment from 'moment-hijri'

class Overlay extends Component {
  constructor(props) {
    super(props)

    this.state = {
      overlayTitle: ' ... ',
    }
  }

  componentDidMount() {}

  componentWillReceiveProps(nextProps) {
    if (nextProps.overlayTitle !== this.state.overlayTitle) {
      this.setState({ overlayTitle: nextProps.overlayTitle })
    }
  }

  componentWillUnmount() {}

  render() {
    return (
      <div className="Overlay">
        <div>{moment().format('dddd, DD MMMM YYYY')}</div>
        <div>{moment().format('iDD iMMMM iYYYY')}</div>
        <h1>{this.state.overlayTitle}</h1>
      </div>
    )
  }
}

Overlay.propTypes = {
  overlayTitle: PropTypes.string,
}

export default Overlay
